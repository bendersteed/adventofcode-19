intcode_test = [1,9,10,3,2,3,11,0,99,30,40,50]

def run_intcode(intcode):
    i = 0
    while i != 99:
        if intcode[i] == 1:
            intcode[intcode[i+3]] = intcode[intcode[i+1]] + intcode[intcode[i+2]]
            i = i + 4
        elif intcode[i] == 2:
            intcode[intcode[i+3]] = intcode[intcode[i+1]] * intcode[intcode[i+2]]
            i = i + 4
        elif intcode[i] == 99:
            i = 99
            print("the program is over!")
        else:
            i = 99
            print("there was some error with the intcode")

def challenge1(intcode):
    temp_intcode = intcode.copy()
    temp_intcode[1] = 12
    temp_intcode[2] = 2
    run_intcode(temp_intcode)
    print(f'the answer is {temp_intcode[0]}')

def challenge2(intcode):
    for i in range(0, 100):
        for j in range(0, 100):
            temp_intcode = intcode.copy()
            temp_intcode[1] = i
            temp_intcode[2] = j
            run_intcode(temp_intcode)
            if temp_intcode[0] == 19690720:
                print(f'the verb is {i} and the noun is {j}, so the answer is {100 * i + j}')
                break
        else:
            continue
        break
