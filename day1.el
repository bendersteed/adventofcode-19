(defun fuel (mass)
  (let ((f (- (floor (/ mass 3)) 2)))
    (if (> f 0)
	f
      0)))

(defun fuel-all (mass)
  (let ((f (fuel mass)))
   (if (= f 0)
       0
     (+ f (fuel-all f)))))

(defun challlenge1 (input)
  (apply '+ (mapcar 'fuel input)))

(defun challenge2 (input)
 (apply '+ (mapcar 'fuel-all input)))
